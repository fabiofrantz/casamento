activate_this = '/home/staging/sites/casamento/env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import sys
sys.path.insert(0, '/home/staging/sites/casamento/')

from server import app as application